// JavaScript Document

$(document).ready( function(){
	
	// Área de busca
	strBuscaDefault = "Buscar no Site";
	
	if($("#s").val().replace(" ","")=="") $("#s").val(strBuscaDefault);
	
	$("#s").click( function(){
		if($("#s").val() == strBuscaDefault || $("#s").val().replace(" ","")=="") $("#s").val("");										  
	});
	
	$("#s").blur( function(){
		if($("#s").val().replace(" ","")=="") $("#s").val(strBuscaDefault);										  
	});
	
	
	// Slides	
	if($(".slides").size()!=0){ 
	
		for(auxCont=($(".slides").size()-1); auxCont>=1; auxCont--){
			
			$(".slides:eq(" + auxCont +")").delay(($(".slides").size()-1- auxCont)*1600).slideUp(300);
		}
		
		auxContSlide = 0;
		
		function rodaSlide(){
			auxContSlide++;
			if(auxContSlide > $(".slides").size()-1) auxContSlide = 0;
			$(".slides").fadeOut(100).css("display","none");
			$("#nav li").removeClass("atual");
			$("#nav li:eq(" + auxContSlide + ")").addClass("atual");
			$(".slides:eq(" + auxContSlide + ")").fadeIn(600).css("display","block");			
		}
		
		$("#nav li:eq(0)").addClass("atual");
		
		sliderIntervalID = window.setInterval(rodaSlide, 50000);
		
		$(".ativaSlide").click( function(index){
			$("#nav li").removeClass("atual");
			$(this).parent("li").addClass("atual");
			window.clearInterval(sliderIntervalID);
			auxContSlide = $(this).text()-1;
			$(".slides").fadeOut(100).css("display","none");
			$(".slides:eq(" + ($(this).text()-1) + ")").fadeIn(600).css("display","block");			
			sliderIntervalID = window.setInterval(rodaSlide, 50000);
		});
		
	}
	
	
	$(".data-lista").each(function(){								   
		if($(this).text()=="") $(this).parent("p").parent(".boxproduto-thumb").parent(".boxlistanovidades").css("border","none");
	});
	
	
});