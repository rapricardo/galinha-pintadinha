<?php

global $project;
$project = 'mysite';

global $databaseConfig;
$databaseConfig = array(
	"type" => 'MySQLDatabase',
	"server" => 'localhost',
	"username" => 'root',
	"password" => '',
	//"database" => 'galinha1',
	"database" => 'galinhav3',
	"path" => '',
);

//Force enviroment to Dev ** REMOVE FOR LIVE SITES **
Director::set_environment_type("dev");

MySQLDatabase::set_connection_charset('utf8');

// This line set's the current theme. More themes can be
// downloaded from http://www.silverstripe.org/themes/
SSViewer::set_theme('galinha');

// Set the site locale
i18n::set_locale('pt_BR');
i18n::set_date_format('pt_BR');
i18n::set_time_format('pt_BR');
setlocale(LC_TIME, 'pt_BR.UTF8');

// enable nested URLs for this site (e.g. page/sub-page/)
SiteTree::enable_nested_urls();

//Force enviroment to Dev ** REMOVE FOR LIVE SITES **
#Director::set_environment_type("dev");
//
//Force cache to flush on page load if in Dev mode (prevents needing ?flush=1 on the end of a URL)
if (Director::isDev()) {
    SSViewer::flush_template_cache();

    //Set default login
Security::setDefaultAdmin('admin','e5q8n6k3');
//
//Define the password validator
$pwdValidator = new PasswordValidator();
$pwdValidator->minLength(8);
$pwdValidator->checkHistoricalPasswords(2);
$pwdValidator->characterStrength(2,array('lowercase','digits'));
Member::set_password_validator($pwdValidator);
}
Director::isLive();
if(Director::isLive()) Debug::send_errors_to("leo@altonivel.com.br");
//Enable Search, use $SearchForm in template
FulltextSearchable::enable();

// Set the resizing image quality
GD::set_default_quality(95);

Director::addRules(100, array('produtos/$Action/$URLSegment' => 'ProdutoHolder_Controller'));
