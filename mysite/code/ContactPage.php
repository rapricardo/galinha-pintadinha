<?php
/******************
 * 
 * ContactPage
 * 
 * Tutorial on www.ssbits.com/creating-a-simple-contact-form/
 * 
 * Author: Aram Balakjian of aabweb.co.uk
 * 
 ******************/

//Model
class ContactPage extends Page
{
	static $db = array(
		'Mailto' => 'Varchar(100)', //Email address to send submissions to
		'SubmitText' => 'Text' //Text presented after submitting message
	);
	
	//CMS fields
	function getCMSFields() 
	{
		$fields = parent::getCMSFields();
	
		$fields->addFieldToTab("Root.Content.OnSubmission", new TextField('Mailto', 'Email submissions to'));	
		$fields->addFieldToTab("Root.Content.OnSubmission", new TextareaField('SubmitText', 'Text on Submission'));	
	
		return $fields;	
	}

}

// Controller
class ContactPage_Controller extends Page_Controller
{
	//Define our form function as allowed
	static $allowed_actions = array(
		'ContactForm'
	);
	
	//The function which generates our form
	function ContactForm() 
	{
      	// Create fields
	    $fields = new FieldSet(
		    new TextField('Name', 'Nome*','',90),
			new EmailField('Email', 'E-mail*','',90),
			new DropdownField('Subject','Assunto*',array(
			'Informações sobre pedidos'=> 'Informações sobre pedidos',
			'Dúvidas sobre a loja'=> 'Dúvidas sobre a loja',
			'Informações sobre produtos para logistas'=> 'Informações sobre produtos para logistas',
			'Contato para Shows'=> 'Contato para Shows',
			'Contato para o Cineminha da Galinha Pintadinha'=> 'Contato para o Cineminha da Galinha Pintadinha',
			'Contato para projeto Brincando com a Galinha Pintadinha'=> 'Contato para projeto Brincando com a Galinha Pintadinha',
			'Licenciamentos'=> 'Licenciamentos',
			'Outros'=> 'Outros'
	       )),			
			new TextareaField('Comments','Comentário*',10,40)
		);
	 	
	    // Create action
	    $actions = new FieldSet(
	    	new FormAction('SendContactForm', 'Enviar')
	    );
		
		// Create action
		$validator = new RequiredFields('Name', 'Email', 'Comments');
			
	    //return new Form($this, 'ContactForm', $fields, $actions, $validator);
           $form_contact = new Form($this, 'ContactForm', $fields, $actions, $validator);
           $form_contact->disableSecurityToken();
           return $form_contact;
	}
 	
	//The function that handles our form submission
	function SendContactForm($data, $form) 
	{
	 	
	//Set data
		$From = $data['Email'];
		$To = $this->Mailto;
		$Subject = "Contato Site - Galinha Pintadinha " . $data['Subject'];  	  
		$email = new Email($From, $To, $Subject);
		//set template
		$email->setTemplate('ContactEmail');
		//populate template
		$email->populateTemplate($data);
		//send mail
		$email->send();
	  	//return to submitted message
		Director::redirect(Director::baseURL(). $this->URLSegment . "/?success=1");

	}

	//The function to test whether to display the Submit Text or not
	public function Success()
	{
		return isset($_REQUEST['success']) && $_REQUEST['success'] == "1";
	}
	public function init() {
		parent::init();
        i18n::set_locale('pt_BR');
        i18n::set_date_format('pt_BR');
        i18n::set_time_format('pt_BR');
        setlocale(LC_TIME, 'pt_BR.UTF8');
	}
	
}