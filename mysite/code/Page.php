<?php
class Page extends SiteTree {

	public static $db = array(
        "Novidade" => "Boolean",
        "Promo" => "Boolean",
        "Omega" => "Boolean",
        "Resumo" => "Text",
        "Link_itunes" => "Text",
        "Extra" => "HTMLText",
	);

	public static $has_one = array(
        "Banner" => "Image",
		"Thumb" => "Image",
	);

    public static $has_many = array(
		"Arquivos" => "Arquivo",
	);

    public function getCMSFields()
	{
		$fields = parent::getCMSFields();

		//$fields->addFieldToTab("Root.Content.Imagens", new FileIframeField('Swf','Swf') );

        $arquivos = new FileDataObjectManager( $this, 'Arquivos', 'Arquivo' , 'File');
        //$fields->addFieldToTab('Root.Content.Imagens', $arquivos);

		$fields->addFieldToTab("Root.Content.Banners", new FileIframeField('Banner','Banner') );
		$fields->addFieldToTab("Root.Content.Banners", new FileIframeField('Thumb','Thumb') );
        //$fields->addFieldToTab("Root.Content.Main", new CheckboxField('Novidade','Novidade') );
        //$fields->addFieldToTab("Root.Content.Main", new CheckboxField('Promo','Promo') );
        //$fields->addFieldToTab("Root.Content.Main", new CheckboxField('Omega','Omega') );
        $fields->addFieldToTab("Root.Content.Main", new TextField("Resumo", "Resumo"),'Content');
        //$fields->addFieldToTab("Root.Content.Main", new TextField("Link_itunes", "Link_itunes") );
		return $fields;
	}

    function TagsProduto (  )
    {
        return DataObject::get('Tag');

     }

}
class Page_Controller extends ContentController {

	public static $allowed_actions = array (
	);

	public function init() {
		parent::init();
        i18n::set_locale('pt_BR');
        i18n::set_date_format('pt_BR');
        i18n::set_time_format('pt_BR');
        setlocale(LC_TIME, 'pt_BR.UTF8');
	}
}