<?php
class CarrosselHome extends RedirectorPage {

	public static $db = array(
	);

	public static $has_one = array(
	);


    static $defaults = array(
        'ShowInSearch' => false,
    );

    function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->removeFieldFromTab("Root.Content.Banners","Thumb");
        $fields->removeFieldFromTab("Root.Content.Main","Content");
        /*$fields->removeFieldFromTab("Root.Content.Main","Promo");
        $fields->removeFieldFromTab("Root.Content.Main","Omega");
        $fields->removeFieldFromTab("Root.Content.Main","Resumo");*/
        $fields->removeFieldFromTab("Root.Content.Main","Link_itunes");

        return $fields;
     }

}
class CarrosselHome_Controller extends Page_Controller {

	public static $allowed_actions = array (
	);

	public function init() {
		parent::init();

	}
}