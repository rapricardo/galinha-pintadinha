<?php
class Arquivo extends DataObject
{

	static $has_one = array (
        'Page' => 'Page',
        'File' => 'File'
	);

	public function getCMSFields_forPopup()
	{
		return new FieldSet(
          	new FileUploadField('File' ,'File')
		);
	}
}