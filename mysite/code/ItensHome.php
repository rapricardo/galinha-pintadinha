<?php
class ItensHome extends Page {

	public static $db = array(
	);

	public static $has_one = array(
	);

    static $allowed_children = array(
		'BotaoHome','BannerHome','CarrosselHome', 'ItensHome'
	);
    static $defaults = array(
        'ShowInMenus' => false,
        'ShowInSearch' => false,
    );

    function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->removeFieldFromTab("Root.Content.Main","Content");
        $fields->removeFieldFromTab("Root.Content.Main","Novidade");
        $fields->removeFieldFromTab("Root.Content.Main","Promo");
        $fields->removeFieldFromTab("Root.Content.Main","Omega");
        $fields->removeFieldFromTab("Root.Content.Main","Resumo");
        $fields->removeFieldFromTab("Root.Content.Main","Link_itunes");

        return $fields;
     }

}
class ItensHome_Controller extends Page_Controller {

	public static $allowed_actions = array (
	);

	public function init() {
		parent::init();

	}
}